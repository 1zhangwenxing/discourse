#!/bin/bash
if [ x"" = x$1 ] || [ x"" = x$2 ] 
then
	echo params error
	exit
fi
home=`pwd`
home=$home/plugins

src_path=$home/discourse-openid-connect

dest_file=$1
dest_path=$home/$dest_file
target_name=$2
cp -r $src_path $dest_path
cd $dest_path

find $dest_path -type f | xargs sed -i "s/discourse-openid-connect/$dest_file/g" 
find $dest_path -type f | xargs sed -i "s/openid_connnect/$2_openid_connect/g" $dest_path/*
find $dest_path -type f | xargs sed -i "s/lib\/$2_openid_connect/lib\/openid_connect/g" $dest_path/*


