# -*- encoding: utf-8 -*-
# stub: discourse-fonts 0.0.9 ruby lib

Gem::Specification.new do |s|
  s.name = "discourse-fonts".freeze
  s.version = "0.0.9"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Bianca Nenciu".freeze]
  s.date = "2021-08-11"
  s.description = "Bundle of fonts which can be used to customize the look of Discourse".freeze
  s.email = "bianca.nenciu@discourse.org".freeze
  s.homepage = "https://github.com/discourse/discourse-fonts".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5.0".freeze)
  s.rubygems_version = "3.2.31".freeze
  s.summary = "Bundle of customizable Discourse fonts".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_development_dependency(%q<rubocop-discourse>.freeze, ["~> 2.4.1"])
  else
    s.add_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_dependency(%q<rubocop-discourse>.freeze, ["~> 2.4.1"])
  end
end
