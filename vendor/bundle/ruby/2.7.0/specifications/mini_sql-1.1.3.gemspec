# -*- encoding: utf-8 -*-
# stub: mini_sql 1.1.3 ruby lib

Gem::Specification.new do |s|
  s.name = "mini_sql".freeze
  s.version = "1.1.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "bug_tracker_uri" => "https://github.com/discourse/mini_sql/issues", "changelog_uri" => "https://github.com/discourse/mini_sql/blob/master/CHANGELOG", "source_code_uri" => "https://github.com/discourse/mini_sql" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Sam Saffron".freeze]
  s.date = "2021-03-23"
  s.description = "A fast, safe, simple direct SQL executor for PG".freeze
  s.email = ["sam.saffron@gmail.com".freeze]
  s.homepage = "https://github.com/discourse/mini_sql".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.2.31".freeze
  s.summary = "A fast, safe, simple direct SQL executor".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<bundler>.freeze, ["> 1.16"])
    s.add_development_dependency(%q<rake>.freeze, ["> 10"])
    s.add_development_dependency(%q<minitest>.freeze, ["~> 5.0"])
    s.add_development_dependency(%q<guard>.freeze, ["~> 2.14"])
    s.add_development_dependency(%q<guard-minitest>.freeze, ["~> 2.4"])
    s.add_development_dependency(%q<activesupport>.freeze, ["~> 5.2"])
    s.add_development_dependency(%q<rubocop>.freeze, ["~> 1.4.0"])
    s.add_development_dependency(%q<rubocop-discourse>.freeze, ["~> 2.4.1"])
    s.add_development_dependency(%q<m>.freeze, ["~> 1.5.1"])
    s.add_development_dependency(%q<pg>.freeze, ["> 1"])
    s.add_development_dependency(%q<mysql2>.freeze, [">= 0"])
    s.add_development_dependency(%q<sqlite3>.freeze, ["~> 1.3"])
  else
    s.add_dependency(%q<bundler>.freeze, ["> 1.16"])
    s.add_dependency(%q<rake>.freeze, ["> 10"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.0"])
    s.add_dependency(%q<guard>.freeze, ["~> 2.14"])
    s.add_dependency(%q<guard-minitest>.freeze, ["~> 2.4"])
    s.add_dependency(%q<activesupport>.freeze, ["~> 5.2"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 1.4.0"])
    s.add_dependency(%q<rubocop-discourse>.freeze, ["~> 2.4.1"])
    s.add_dependency(%q<m>.freeze, ["~> 1.5.1"])
    s.add_dependency(%q<pg>.freeze, ["> 1"])
    s.add_dependency(%q<mysql2>.freeze, [">= 0"])
    s.add_dependency(%q<sqlite3>.freeze, ["~> 1.3"])
  end
end
