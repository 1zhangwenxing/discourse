# -*- encoding: utf-8 -*-
# stub: xorcist 1.1.2 ruby lib
# stub: ext/xorcist/extconf.rb

Gem::Specification.new do |s|
  s.name = "xorcist".freeze
  s.version = "1.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Faraz Yashar".freeze]
  s.cert_chain = ["-----BEGIN CERTIFICATE-----\nMIIDgDCCAmigAwIBAgIBATANBgkqhkiG9w0BAQUFADBDMRUwEwYDVQQDDAxmYXJh\nei55YXNoYXIxFTATBgoJkiaJk/IsZAEZFgVnbWFpbDETMBEGCgmSJomT8ixkARkW\nA2NvbTAeFw0xODAyMjcwMzE2MTFaFw0xOTAyMjcwMzE2MTFaMEMxFTATBgNVBAMM\nDGZhcmF6Lnlhc2hhcjEVMBMGCgmSJomT8ixkARkWBWdtYWlsMRMwEQYKCZImiZPy\nLGQBGRYDY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv9y0yeyB\nvsiW6FTsJk7g0jt0A/emMqN4MUHXJQDGmUnlFSHNqfXUJiXuZanE8ZCnS6++0ouS\nHUrzrbc3Gbw7rNj9/aQpm7SS3Lo70djbQlGwZq1tUteXTWzp90lwI4IiWaWTCRQP\nAgwawkvHYnDLC/iw/F/CZLiaJG4ZLhYBd1DxCEgz3qrwD3G1d35+tj4vbqEqtx/e\nUHtNbj86e3WTPYAJ6MWAk+qLDVX9JiDaIAzW42yRed6FrJexZunsuW9atTdeAUTr\nSJJYYL1agJRgX3SefW/o/tKsBrl/tG7lHIAH4TopXHX4aXoSotWSqI+v4pRnoV3v\nu2kusE3YZDN+jwIDAQABo38wfTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIEsDAdBgNV\nHQ4EFgQUJzDdPthnahTQPLPK2vZmKE9VcN0wIQYDVR0RBBowGIEWZmFyYXoueWFz\naGFyQGdtYWlsLmNvbTAhBgNVHRIEGjAYgRZmYXJhei55YXNoYXJAZ21haWwuY29t\nMA0GCSqGSIb3DQEBBQUAA4IBAQA8vHFCyzGOZkLX48NdnbSp9/1PNmt65ABxjiXV\ndLmlBVfag1//6tU1hdxvfwrFj/6p0J1Z17MY0/zQerTK9hGT4PWOc05ASmCADDFX\nigA/4xc3Z6L+3IM5gmcUjkZEwCq4QxCW5+LlWAO3XYoInufiA7xpjaDGtarZLIJw\n8tWNtyKkYHdUkWOf7mARiqKZmzc+z9MbBS6+RnyjpVjSc3VSNVl2RNOyQuV4bHcU\nEWAzos45LBmn8UZPzUb/szfC1PTzuQp4R7lUCOqi7ExaZF0oYg7JqkRtHDhV/K/c\nE0Og3eJcioAAuEM3ujecxGYxFkG7zDNswn47lCy6xVzBk4iF\n-----END CERTIFICATE-----\n".freeze]
  s.date = "2018-08-03"
  s.description = "Blazing-fast-cross-platform-monkey-patch-free string XOR. Yes, that means JRuby too.".freeze
  s.email = ["faraz.yashar@gmail.com".freeze]
  s.extensions = ["ext/xorcist/extconf.rb".freeze]
  s.files = ["ext/xorcist/extconf.rb".freeze]
  s.homepage = "https://github.com/fny/xorcist".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.2.31".freeze
  s.summary = "Blazing-fast-cross-platform-monkey-patch-free string XOR".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<bundler>.freeze, ["~> 1.10"])
    s.add_development_dependency(%q<minitest>.freeze, ["~> 5.7"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_development_dependency(%q<rake-compiler>.freeze, ["~> 0.9.5"])
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.10"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.7"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.9.5"])
  end
end
