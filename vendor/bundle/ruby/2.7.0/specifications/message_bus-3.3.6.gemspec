# -*- encoding: utf-8 -*-
# stub: message_bus 3.3.6 ruby lib

Gem::Specification.new do |s|
  s.name = "message_bus".freeze
  s.version = "3.3.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Sam Saffron".freeze]
  s.date = "2021-05-31"
  s.description = "A message bus for rack".freeze
  s.email = ["sam.saffron@gmail.com".freeze]
  s.homepage = "https://github.com/SamSaffron/message_bus".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.4.0".freeze)
  s.rubygems_version = "3.2.31".freeze
  s.summary = "".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<rack>.freeze, [">= 1.1.3"])
    s.add_development_dependency(%q<redis>.freeze, [">= 0"])
    s.add_development_dependency(%q<pg>.freeze, [">= 0"])
  else
    s.add_dependency(%q<rack>.freeze, [">= 1.1.3"])
    s.add_dependency(%q<redis>.freeze, [">= 0"])
    s.add_dependency(%q<pg>.freeze, [">= 0"])
  end
end
