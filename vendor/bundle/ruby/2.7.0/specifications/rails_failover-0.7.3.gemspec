# -*- encoding: utf-8 -*-
# stub: rails_failover 0.7.3 ruby lib

Gem::Specification.new do |s|
  s.name = "rails_failover".freeze
  s.version = "0.7.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Alan Tan".freeze]
  s.bindir = "exe".freeze
  s.date = "2021-04-15"
  s.email = ["tgx@discourse.org".freeze]
  s.homepage = "https://github.com/discourse/rails_failover".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5.0".freeze)
  s.rubygems_version = "3.2.31".freeze
  s.summary = "Failover for ActiveRecord and Redis".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<activerecord>.freeze, ["~> 6.0"])
    s.add_runtime_dependency(%q<railties>.freeze, ["~> 6.0"])
    s.add_runtime_dependency(%q<concurrent-ruby>.freeze, [">= 0"])
  else
    s.add_dependency(%q<activerecord>.freeze, ["~> 6.0"])
    s.add_dependency(%q<railties>.freeze, ["~> 6.0"])
    s.add_dependency(%q<concurrent-ruby>.freeze, [">= 0"])
  end
end
