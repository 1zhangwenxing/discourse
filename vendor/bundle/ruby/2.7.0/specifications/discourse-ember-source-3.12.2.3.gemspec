# -*- encoding: utf-8 -*-
# stub: discourse-ember-source 3.12.2.3 ruby lib

Gem::Specification.new do |s|
  s.name = "discourse-ember-source".freeze
  s.version = "3.12.2.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Joffrey JAFFEUX".freeze]
  s.bindir = "exe".freeze
  s.date = "2021-03-11"
  s.description = "Fork of Ember source to permit latest ember versions".freeze
  s.email = ["j.jaffeux@gmail.com".freeze]
  s.homepage = "https://github.com/discourse/discourse-ember-source".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5.0".freeze)
  s.rubygems_version = "3.2.31".freeze
  s.summary = "Fork of Ember source".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<bundler>.freeze, [">= 1.16"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_development_dependency(%q<minitest>.freeze, ["~> 5.0"])
    s.add_development_dependency(%q<rubocop-discourse>.freeze, ["~> 2.4.1"])
  else
    s.add_dependency(%q<bundler>.freeze, [">= 1.16"])
    s.add_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.0"])
    s.add_dependency(%q<rubocop-discourse>.freeze, ["~> 2.4.1"])
  end
end
