# -*- encoding: utf-8 -*-
# stub: rails_multisite 4.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "rails_multisite".freeze
  s.version = "4.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Sam Saffron".freeze]
  s.date = "2021-11-15"
  s.description = "Multi tenancy support for Rails".freeze
  s.email = ["sam.saffron@gmail.com".freeze]
  s.homepage = "".freeze
  s.required_ruby_version = Gem::Requirement.new(">= 2.4.0".freeze)
  s.rubygems_version = "3.2.31".freeze
  s.summary = "Multi tenancy support for Rails".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<activerecord>.freeze, ["> 5.0", "< 7"])
    s.add_runtime_dependency(%q<railties>.freeze, ["> 5.0", "< 7"])
  else
    s.add_dependency(%q<activerecord>.freeze, ["> 5.0", "< 7"])
    s.add_dependency(%q<railties>.freeze, ["> 5.0", "< 7"])
  end
end
