# -*- encoding: utf-8 -*-
# stub: cppjieba_rb 0.3.3 ruby lib
# stub: ext/cppjieba_rb/extconf.rb

Gem::Specification.new do |s|
  s.name = "cppjieba_rb".freeze
  s.version = "0.3.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Erick Guan".freeze]
  s.date = "2018-09-26"
  s.description = "cppjieba binding for ruby. Mainly used by Discourse.".freeze
  s.email = ["fantasticfears@gmail.com".freeze]
  s.extensions = ["ext/cppjieba_rb/extconf.rb".freeze]
  s.files = ["ext/cppjieba_rb/extconf.rb".freeze]
  s.homepage = "https://github.com/fantasticfears/cppjieba_rb".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.3.0".freeze)
  s.rubygems_version = "3.2.31".freeze
  s.summary = "cppjieba binding for ruby".freeze

  s.installed_by_version = "3.2.31" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<bundler>.freeze, ["~> 1.5"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 12"])
    s.add_development_dependency(%q<rake-compiler>.freeze, ["~> 1"])
    s.add_development_dependency(%q<minitest>.freeze, ["~> 5.11"])
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.5"])
    s.add_dependency(%q<rake>.freeze, ["~> 12"])
    s.add_dependency(%q<rake-compiler>.freeze, ["~> 1"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.11"])
  end
end
